//
//  HomeViewController.swift
//  MusicPlayer
//
//  Created by Sunil Gurung on 9/8/19.
//  Copyright © 2019 Sunil Gurung. All rights reserved.
//

import UIKit
import UIKit
import AVFoundation

class HomeViewController: UIViewController,AVAudioPlayerDelegate {

    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnPause: UIButton!
    var soundPlayer = AVAudioPlayer()
    var timer:Timer!
    @IBOutlet var lblTrackName: UILabel!
    var hasBeenPaused = false
    var TrackName = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        playList()
        btnPause.isHidden = true
        TrackName = ["Darkside"]
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        self.navigationItem.hidesBackButton = true

    }
    
    
    @IBAction func btnSetting(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let settingVC = storyBoard.instantiateViewController(withIdentifier: "settingVC") as! SettingViewController
        self.navigationController?.pushViewController(settingVC, animated: true)
    }
    
    @IBAction func btnPrevious(_ sender: Any) {
        soundPlayer.stop()
    }
    
    @IBAction func btnPlay(_ sender: Any) {
        soundPlayer.play()
        btnPlay.isHidden = true
        btnPause.isHidden = false
    }
    
    @IBAction func btnPause(_ sender: Any) {
        if soundPlayer.isPlaying{
            soundPlayer.pause()
            hasBeenPaused = true
            btnPause.isHidden = true
            btnPlay.isHidden = false
        } else{
            hasBeenPaused = false
        }
    }
    @IBAction func btnNext(_ sender: Any) {
        if soundPlayer.isPlaying || hasBeenPaused{
            soundPlayer.stop()
            soundPlayer.currentTime = 0
            soundPlayer.play()
        } else{
            soundPlayer.play()
        }
        
    }
    
    func playList(){
        do{
            soundPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "Darkside", ofType: "mp3")!))
            soundPlayer.prepareToPlay()
            
            let audioSession = AVAudioSession.sharedInstance()
            do{
                try audioSession.setCategory(.playback, mode: .default)
                try audioSession.setActive(true)
                    lblTrackName.text = "Darkside"
                
            } catch {
                print(error)
            }
            
        }catch let playListError{
            print(playListError)
        }
    }

    func end(){
        if soundPlayer.currentTime == 0 {
            btnPause.isHidden = false
            btnPlay.isHidden = true
        }
        
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        if player.stop() == true{
            btnPause.isHidden = false
            btnPlay.isHidden = true
        }
        
    }
}
