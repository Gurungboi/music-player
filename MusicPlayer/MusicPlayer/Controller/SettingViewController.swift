//
//  SettingViewController.swift
//  MusicPlayer
//
//  Created by Sunil Gurung on 9/8/19.
//  Copyright © 2019 Sunil Gurung. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {

    var name = [String]()
    var imgicon = [String]()
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let fbimage = UserDefaults.standard.string(forKey: "fbimage")

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        infodetail()
        name = ["About Us","Sign Out"]
        imgicon = ["play-50","pause-50"]
        self.tableView.tableFooterView = UIView()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    /************************* METHODS *************************/
    func infodetail(){
        let name = UserDefaults.standard.string(forKey: "name")
        let email = UserDefaults.standard.string(forKey: "email")
        let fbimage = UserDefaults.standard.string(forKey: "fbimage")
        lblName.text = name
        lblEmail.text = email
        
        let imgURL = URL(string: fbimage!)
        if imgURL != nil {
            let data = try? Data(contentsOf: imgURL!)
            imgProfile.image = UIImage(data:data!)
        }else{return}
    }
    func logout(){
        let alertVC = UIAlertController(
            title: "Are you sure you want to log out?",
            message: "Logging out will delete all the information",
            preferredStyle: .actionSheet)

        let logoutAction = UIAlertAction(
            title: "Log Out",
            style:.default,
            handler:{ action in
                self.out()
        })
        
        alertVC.addAction(logoutAction)
        
        let cancelAction = UIAlertAction(
            title: "Cancel",
            style:.cancel,
            handler: nil)
        alertVC.addAction(cancelAction)
        
        present(
            alertVC,
            animated: true,
            completion: nil)
    }
    
    func out(){
        //DatabaseHandler().deleteObject()
        let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let rootViewController = storyboard.instantiateViewController(withIdentifier: "loginVC") as! LoginViewController
        navigationController.viewControllers = [rootViewController]
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = navigationController
        fbLoginManager.logOut()
    }
    
}

extension SettingViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingTableViewCell
        let indexpath = indexPath.row
        cell.lblName.text = name[indexpath]
        cell.imgIcon.image = UIImage(named:imgicon[indexpath])
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row
        if index == 0{
            self.title = ""
        }
        else if index == 1 {
            self.logout()
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
