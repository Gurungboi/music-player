//
//  LoginViewController.swift
//  MusicPlayer
//
//  Created by Sunil Gurung on 9/8/19.
//  Copyright © 2019 Sunil Gurung. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit

let fbLoginManager : LoginManager = LoginManager()
class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func btnSkip(_ sender: Any) {
        dashboard()
    }
    @IBAction func btnLogin(_ sender: Any) {
        facebookLogin()
    }
    func facebookLogin(){
        fbLoginManager.logIn(permissions: ["email","user_friends"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email") && fbloginresult.grantedPermissions.contains("user_friends"))
                    {
                        self.getFBUserData()
                    }else{
                    }
                }
                else{
                    self.dashboard()
                }
            }
        }
    }
    func getFBUserData(){
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard
        let accessToken = AccessToken.current
        guard let accessTokenString = accessToken?.tokenString else{return}
        token.set(accessTokenString, forKey: "accessTokens")
        token.synchronize()
        
//        let credential = AuthProvider.credential(withAccessToken: accessTokenString)
//
//        Auth.auth().signIn(with: credential) { (user, error) in
//            if (error != nil){
//                print("Something is Error",error ?? "")
//                return
//            }
//
//        }
        if((accessToken) != nil){
            let params = ["fields": "id, first_name, last_name, name, email, picture.type(large)"]
            GraphRequest(graphPath: "me", parameters: params).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    var dict : [String : AnyObject]!
                    dict = result as? [String : AnyObject]
                    let imageDict : [String : AnyObject]!
                    if (dict.count > 0)
                    {
                        let data: AnyObject = dict["picture"]!
                        imageDict = data as? [String : AnyObject]
                        let imageData: AnyObject = imageDict["data"]!
                        defaults.setValue(dict["id"]! as? String, forKey: "appuserid")
                        defaults.setValue(dict["name"]! as? String, forKey: "name")
                        defaults.setValue(dict["email"]! as? String, forKey: "email")
                        defaults.setValue(imageData["url"]!, forKey: "fbimage")
                    }
                    self.dashboard()
                }else{
                    print("Error in Access Toke")
                }
            })
            //me/taggable_friends
            let graphRequest = GraphRequest(graphPath: "/me/friends", parameters: params)
            let connection = GraphRequestConnection()
            connection.add(graphRequest, completionHandler: { (connection, result, error) in
                if error == nil {
                    if let userData = result as? [String:Any] {
                        var dict : [String : AnyObject]!
                        dict = userData as [String : AnyObject]
                        if dict.count > 0{
                            let array = dict["data"] as! NSArray
                            defaults.setValue(array, forKey: "fbfriendlist")
                        }
                    }
                } else {
                    print("Error Getting Friends \(String(describing: error))");
                }
            })
            connection.start()
        }
    }
    
    func dashboard(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeVC = storyBoard.instantiateViewController(withIdentifier: "homeVC") as! HomeViewController
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
}
